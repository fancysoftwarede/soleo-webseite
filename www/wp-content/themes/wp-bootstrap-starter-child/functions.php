<?php
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

    wp_enqueue_script('customt', get_stylesheet_directory_uri() .'/custom.js');
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

add_shortcode('wpbsearch', 'get_search_form');


add_shortcode( 'projekte', 'display_custom_post_type' );

function display_custom_post_type(){
    $args = array(
        'cat' => 17, // Kategorie Projekte
        'post_status' => 'publish'
    );

    $string = '';
    $query = new WP_Query( $args );
    if( $query->have_posts() ){
        $string .= '
<div class="spalten table-heading">
<div>Projekt</div>
<div>Ort</div>
<div>Projekttyp</div>
</div><div id="accordion" role="tablist" aria-multiselectable="false">';
        while( $query->have_posts() ){
            $query->the_post();
            $id = $query->post->ID;

            $header = '<div class="card-header" role="tab" id="headingOne">
<h5 class="mb-0">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$id.'" aria-expanded="false" aria-controls="collapse'.$id.'" class="collapsed"><p></p>
<div class="spalten">
<div>'.get_the_title().'</div>
<div>'.get_post_custom_values( 'auftraggeber' )[0].'</div>
<div>'.get_post_custom_values( 'leistung' )[0].'</div>
<p></p></div>
</a><p><a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$id.'" aria-expanded="false" aria-controls="collapse'.$id.'" class="collapsed">        </a></p></h5>
<p></p></div>';


            $string .= '<div class="card">' . $header . '<div id="collapse'.$id.'" class="collapse" role="tabpanel" aria-labelledby="heading'.$id.'" aria-expanded="false" style="">
<div class="card-block">'.$query->post->post_content.'</div></div>' . '</div>';
        }
        $string .= '</div>';
    }
    wp_reset_postdata();
    return $string;
}