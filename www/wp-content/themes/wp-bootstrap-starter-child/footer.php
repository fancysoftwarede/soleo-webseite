<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
            	<div class="site-info">
                <a href="/soleo/impressum/" class="footer-link-left" title="Impressum">IMPRESSUM</a> 
		<span class="sep"> . </span>
		<a href="/soleo/kontakt/" class="footer-link-left" title="Kontakt">KONTAKT</a> 
               
                <a href="/soleo/suche" class="footer-link-right" title="Suche">SUCHE</a>
            </div><!-- close .site-info -->
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
